#!/bin/python3

import json
import csv

file_Tip = open("Tip.csv", "w")
Tipwriter = csv.writer(file_Tip)
#Tipwriter.writerow(["text", "date", "likes", "businessID", "userID"])

for i, line in enumerate(open("yelp_academic_dataset_tip.json", "r")):
#  if i >= 1000:
#    break;
  data = json.loads(line)
  
  #Tip
  Tipwriter.writerow([data["text"], data["date"], data["likes"], 
                           data["business_id"], data["user_id"]])
  
  
