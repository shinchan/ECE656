DROP TABLE IF EXISTS Business CASCADE; 
CREATE TABLE Business(
businessID     CHAR(22)  PRIMARY KEY,
name           VARCHAR(64)  NOT NULL CHECK(name <> ''),
neighborhood   VARCHAR(64),
address        VARCHAR(128),
city           VARCHAR(64),
state          VARCHAR(8),
postalCode     VARCHAR(8),
latitude       FLOAT,
longitude      FLOAT,
stars          FLOAT  CHECK(stars in (0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0)),
reviewCount    INT    CHECK(reviewCount >= 0) NOT NULL, 
isOpen         BOOLEAN
);
CREATE INDEX BusinessKI on Business(businessID);


DROP TABLE IF EXISTS BusinessHours CASCADE; 
CREATE TABLE BusinessHours(
businessID  CHAR(22)  REFERENCES Business(businessID) PRIMARY KEY,
mon_open    TIME,
mon_close   TIME,
tue_open    TIME,
tue_close   TIME,
wed_open    TIME,
wed_close   TIME,
thu_open    TIME,
thu_close   TIME,
fri_open    TIME,
fri_close   TIME,
sat_open    TIME,
sat_close   TIME,
sun_open    TIME,
sun_close   TIME
);
CREATE INDEX BusinessHoursKI on BusinessHours(businessID);

DROP TABLE IF EXISTS BusinessAttributeName CASCADE; 
CREATE TABLE BusinessAttributeName(
attrName  VARCHAR(64)  PRIMARY KEY CHECK(attrName <> '')
);

DROP TABLE IF EXISTS BusinessCategoryName CASCADE; 
CREATE TABLE BusinessCategoryName(
catName  VARCHAR(64)  PRIMARY KEY CHECK(catName <> '')
);

DROP TABLE IF EXISTS BusinessAttribute CASCADE; 
CREATE TABLE BusinessAttribute(
businessID  CHAR(22)    REFERENCES Business(businessID) NOT NULL,
attrName    VARCHAR(64) REFERENCES BusinessAttributeName(attrName) NOT NULL,
attrValue   BOOLEAN  NOT NULL,
PRIMARY KEY (businessID, attrName)
);
CREATE INDEX BusinessAttributeKI on BusinessAttribute(businessID);

DROP TABLE IF EXISTS BusinessCategory CASCADE; 
CREATE TABLE BusinessCategory(
businessID  CHAR(22)    REFERENCES Business(businessID) NOT NULL,
catName     VARCHAR(64) REFERENCES BusinessCategoryName(catName) NOT NULL
);
CREATE INDEX BusinessCategoryKI on BusinessCategory(businessID);

DROP TABLE IF EXISTS YelpUser CASCADE; 
CREATE TABLE YelpUser(
userID        CHAR(22)  PRIMARY KEY,
name          VARCHAR(32),
reviewCount   INT  NOT NULL  CHECK(reviewCount >= 0),
yelpingSince  DATE NOT NULL,
useful        INT  NOT NULL  CHECK(useful >= 0),
funny         INT  NOT NULL  CHECK(funny >= 0),
cool          INT  NOT NULL  CHECK(cool >= 0),
fans          INT  NOT NULL  CHECK(fans >= 0),
averageStars  FLOAT NOT NULL CHECK(averageStars >= 0.0 AND averageStars <= 5.0),
compliment_hot      INT  NOT NULL  CHECK(compliment_hot >= 0),
compliment_more     INT  NOT NULL  CHECK(compliment_more >= 0),
compliment_profile  INT  NOT NULL  CHECK(compliment_profile >= 0),
compliment_cute      INT  NOT NULL  CHECK(compliment_cute >= 0),
compliment_list     INT  NOT NULL  CHECK(compliment_list >= 0),
compliment_note     INT  NOT NULL  CHECK(compliment_note >= 0),
compliment_plain    INT  NOT NULL  CHECK(compliment_plain >= 0),
compliment_cool     INT  NOT NULL  CHECK(compliment_cool >= 0),
compliment_funny    INT  NOT NULL  CHECK(compliment_funny >= 0),
compliment_writer   INT  NOT NULL  CHECK(compliment_writer >= 0),
compliment_photos   INT  NOT NULL  CHECK(compliment_photos >= 0)
);
CREATE INDEX YelpUserKI on YelpUser(userID);

DROP TABLE IF EXISTS UserFriend CASCADE; 
CREATE TABLE UserFriend(
userID        CHAR(22)  REFERENCES YelpUser(userID),
friendUserID  CHAR(22),
PRIMARY KEY   (userID, friendUserID)
);
CREATE INDEX UserFriendUserIndex on YelpUser(userID);

DROP TABLE IF EXISTS UserEliteYear CASCADE; 
CREATE TABLE UserEliteYear(
userID      CHAR(22)  REFERENCES YelpUser(userID),
year        INT  CHECK (year >= 1900 AND year <=2017),
PRIMARY KEY   (userID, year)
);
CREATE INDEX UserEliteYearKI on UserFriend(userID);

DROP TABLE IF EXISTS Review CASCADE; 
CREATE TABLE Review(
reviewID    CHAR(22)  PRIMARY KEY,
userID      CHAR(22)  REFERENCES YelpUser(userID)  NOT NULL,
businessID  CHAR(22)  REFERENCES Business(businessID)  NOT NULL,
stars       FLOAT  CHECK(stars in (0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0))  NOT NULL,
date        DATE  NOT NULL,
text        VARCHAR(9126)  NOT NULL  CHECK(text <> ''),
useful      INT  CHECK(useful >= 0)  NOT NULL,
funny       INT  CHECK(funny >= 0)  NOT NULL,
cool        INT  CHECK(cool >= 0)  NOT NULL,
UNIQUE      (userID, businessID)
);
CREATE INDEX ReviewBusinessIndex on Review(businessID);
CREATE INDEX ReviewUserIndex on Review(userID);

DROP TABLE IF EXISTS Checkins CASCADE; 
CREATE TABLE Checkins(
businessID  CHAR(22)  PRIMARY KEY  REFERENCES Business(businessID),
mon_0   INT  CHECK(mon_0 >= 0),
mon_1   INT  CHECK(mon_1 >= 0),
mon_2   INT  CHECK(mon_2 >= 0),
mon_3   INT  CHECK(mon_3 >= 0),
mon_4   INT  CHECK(mon_4 >= 0),
mon_5   INT  CHECK(mon_5 >= 0),
mon_6   INT  CHECK(mon_6 >= 0),
mon_7   INT  CHECK(mon_7 >= 0),
mon_8   INT  CHECK(mon_8 >= 0),
mon_9   INT  CHECK(mon_9 >= 0),
mon_10  INT  CHECK(mon_10 >= 0),
mon_11  INT  CHECK(mon_11 >= 0),
mon_12  INT  CHECK(mon_12 >= 0),
mon_13  INT  CHECK(mon_13 >= 0),
mon_14  INT  CHECK(mon_14 >= 0),
mon_15  INT  CHECK(mon_15 >= 0),
mon_16  INT  CHECK(mon_16 >= 0),
mon_17  INT  CHECK(mon_17 >= 0),
mon_18  INT  CHECK(mon_18 >= 0),
mon_19  INT  CHECK(mon_19 >= 0),
mon_20  INT  CHECK(mon_20 >= 0),
mon_21  INT  CHECK(mon_21 >= 0),
mon_22  INT  CHECK(mon_22 >= 0),
mon_23  INT  CHECK(mon_23 >= 0),
tue_0   INT  CHECK(tue_0 >= 0),
tue_1   INT  CHECK(tue_1 >= 0),
tue_2   INT  CHECK(tue_2 >= 0),
tue_3   INT  CHECK(tue_3 >= 0),
tue_4   INT  CHECK(tue_4 >= 0),
tue_5   INT  CHECK(tue_5 >= 0),
tue_6   INT  CHECK(tue_6 >= 0),
tue_7   INT  CHECK(tue_7 >= 0),
tue_8   INT  CHECK(tue_8 >= 0),
tue_9   INT  CHECK(tue_9 >= 0),
tue_10  INT  CHECK(tue_10 >= 0),
tue_11  INT  CHECK(tue_11 >= 0),
tue_12  INT  CHECK(tue_12 >= 0),
tue_13  INT  CHECK(tue_13 >= 0),
tue_14  INT  CHECK(tue_14 >= 0),
tue_15  INT  CHECK(tue_15 >= 0),
tue_16  INT  CHECK(tue_16 >= 0),
tue_17  INT  CHECK(tue_17 >= 0),
tue_18  INT  CHECK(tue_18 >= 0),
tue_19  INT  CHECK(tue_19 >= 0),
tue_20  INT  CHECK(tue_20 >= 0),
tue_21  INT  CHECK(tue_21 >= 0),
tue_22  INT  CHECK(tue_22 >= 0),
tue_23  INT  CHECK(tue_23 >= 0),
wed_0   INT  CHECK(wed_0 >= 0),
wed_1   INT  CHECK(wed_1 >= 0),
wed_2   INT  CHECK(wed_2 >= 0),
wed_3   INT  CHECK(wed_3 >= 0),
wed_4   INT  CHECK(wed_4 >= 0),
wed_5   INT  CHECK(wed_5 >= 0),
wed_6   INT  CHECK(wed_6 >= 0),
wed_7   INT  CHECK(wed_7 >= 0),
wed_8   INT  CHECK(wed_8 >= 0),
wed_9   INT  CHECK(wed_9 >= 0),
wed_10  INT  CHECK(wed_10 >= 0),
wed_11  INT  CHECK(wed_11 >= 0),
wed_12  INT  CHECK(wed_12 >= 0),
wed_13  INT  CHECK(wed_13 >= 0),
wed_14  INT  CHECK(wed_14 >= 0),
wed_15  INT  CHECK(wed_15 >= 0),
wed_16  INT  CHECK(wed_16 >= 0),
wed_17  INT  CHECK(wed_17 >= 0),
wed_18  INT  CHECK(wed_18 >= 0),
wed_19  INT  CHECK(wed_19 >= 0),
wed_20  INT  CHECK(wed_20 >= 0),
wed_21  INT  CHECK(wed_21 >= 0),
wed_22  INT  CHECK(wed_22 >= 0),
wed_23  INT  CHECK(wed_23 >= 0),
thu_0   INT  CHECK(thu_0 >= 0),
thu_1   INT  CHECK(thu_1 >= 0),
thu_2   INT  CHECK(thu_2 >= 0),
thu_3   INT  CHECK(thu_3 >= 0),
thu_4   INT  CHECK(thu_4 >= 0),
thu_5   INT  CHECK(thu_5 >= 0),
thu_6   INT  CHECK(thu_6 >= 0),
thu_7   INT  CHECK(thu_7 >= 0),
thu_8   INT  CHECK(thu_8 >= 0),
thu_9   INT  CHECK(thu_9 >= 0),
thu_10  INT  CHECK(thu_10 >= 0),
thu_11  INT  CHECK(thu_11 >= 0),
thu_12  INT  CHECK(thu_12 >= 0),
thu_13  INT  CHECK(thu_13 >= 0),
thu_14  INT  CHECK(thu_14 >= 0),
thu_15  INT  CHECK(thu_15 >= 0),
thu_16  INT  CHECK(thu_16 >= 0),
thu_17  INT  CHECK(thu_17 >= 0),
thu_18  INT  CHECK(thu_18 >= 0),
thu_19  INT  CHECK(thu_19 >= 0),
thu_20  INT  CHECK(thu_20 >= 0),
thu_21  INT  CHECK(thu_21 >= 0),
thu_22  INT  CHECK(thu_22 >= 0),
thu_23  INT  CHECK(thu_23 >= 0),
fri_0   INT  CHECK(fri_0 >= 0),
fri_1   INT  CHECK(fri_1 >= 0),
fri_2   INT  CHECK(fri_2 >= 0),
fri_3   INT  CHECK(fri_3 >= 0),
fri_4   INT  CHECK(fri_4 >= 0),
fri_5   INT  CHECK(fri_5 >= 0),
fri_6   INT  CHECK(fri_6 >= 0),
fri_7   INT  CHECK(fri_7 >= 0),
fri_8   INT  CHECK(fri_8 >= 0),
fri_9   INT  CHECK(fri_9 >= 0),
fri_10  INT  CHECK(fri_10 >= 0),
fri_11  INT  CHECK(fri_11 >= 0),
fri_12  INT  CHECK(fri_12 >= 0),
fri_13  INT  CHECK(fri_13 >= 0),
fri_14  INT  CHECK(fri_14 >= 0),
fri_15  INT  CHECK(fri_15 >= 0),
fri_16  INT  CHECK(fri_16 >= 0),
fri_17  INT  CHECK(fri_17 >= 0),
fri_18  INT  CHECK(fri_18 >= 0),
fri_19  INT  CHECK(fri_19 >= 0),
fri_20  INT  CHECK(fri_20 >= 0),
fri_21  INT  CHECK(fri_21 >= 0),
fri_22  INT  CHECK(fri_22 >= 0),
fri_23  INT  CHECK(fri_23 >= 0),
sat_0   INT  CHECK(sat_0 >= 0),
sat_1   INT  CHECK(sat_1 >= 0),
sat_2   INT  CHECK(sat_2 >= 0),
sat_3   INT  CHECK(sat_3 >= 0),
sat_4   INT  CHECK(sat_4 >= 0),
sat_5   INT  CHECK(sat_5 >= 0),
sat_6   INT  CHECK(sat_6 >= 0),
sat_7   INT  CHECK(sat_7 >= 0),
sat_8   INT  CHECK(sat_8 >= 0),
sat_9   INT  CHECK(sat_9 >= 0),
sat_10  INT  CHECK(sat_10 >= 0),
sat_11  INT  CHECK(sat_11 >= 0),
sat_12  INT  CHECK(sat_12 >= 0),
sat_13  INT  CHECK(sat_13 >= 0),
sat_14  INT  CHECK(sat_14 >= 0),
sat_15  INT  CHECK(sat_15 >= 0),
sat_16  INT  CHECK(sat_16 >= 0),
sat_17  INT  CHECK(sat_17 >= 0),
sat_18  INT  CHECK(sat_18 >= 0),
sat_19  INT  CHECK(sat_19 >= 0),
sat_20  INT  CHECK(sat_20 >= 0),
sat_21  INT  CHECK(sat_21 >= 0),
sat_22  INT  CHECK(sat_22 >= 0),
sat_23  INT  CHECK(sat_23 >= 0),
sun_0   INT  CHECK(sun_0 >= 0),
sun_1   INT  CHECK(sun_1 >= 0),
sun_2   INT  CHECK(sun_2 >= 0),
sun_3   INT  CHECK(sun_3 >= 0),
sun_4   INT  CHECK(sun_4 >= 0),
sun_5   INT  CHECK(sun_5 >= 0),
sun_6   INT  CHECK(sun_6 >= 0),
sun_7   INT  CHECK(sun_7 >= 0),
sun_8   INT  CHECK(sun_8 >= 0),
sun_9   INT  CHECK(sun_9 >= 0),
sun_10  INT  CHECK(sun_10 >= 0),
sun_11  INT  CHECK(sun_11 >= 0),
sun_12  INT  CHECK(sun_12 >= 0),
sun_13  INT  CHECK(sun_13 >= 0),
sun_14  INT  CHECK(sun_14 >= 0),
sun_15  INT  CHECK(sun_15 >= 0),
sun_16  INT  CHECK(sun_16 >= 0),
sun_17  INT  CHECK(sun_17 >= 0),
sun_18  INT  CHECK(sun_18 >= 0),
sun_19  INT  CHECK(sun_19 >= 0),
sun_20  INT  CHECK(sun_20 >= 0),
sun_21  INT  CHECK(sun_21 >= 0),
sun_22  INT  CHECK(sun_22 >= 0),
sun_23  INT  CHECK(sun_23 >= 0)
);
CREATE INDEX CheckinsKI on Checkins(businessID);

DROP TABLE IF EXISTS Tip CASCADE; 
CREATE TABLE Tip(
text        VARCHAR(9126)  NOT NULL  CHECK(text <> ''),
date        DATE  NOT NULL,
likes       INT  CHECK(likes >= 0)  NOT NULL,
businessID  CHAR(22)  REFERENCES Business(businessID),
userID      CHAR(22)  REFERENCES YelpUser(userID)
);
CREATE INDEX TipBusinessIndex on Tip(businessID);
CREATE INDEX TipUserIndex on Tip(userID);