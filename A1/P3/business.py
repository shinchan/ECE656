#!/bin/python3

import json
import csv

def parseattr(attrlist):
  l = []
  for s in attrlist:
    i = 0
    while(s[i] != ":" and i < len(s)):
      i += 1
    if s[i+2] == '{':
      s_json = s[i+2:].replace("'", '"').replace(": ", ':"')\
               .replace(", ", '",').replace('}', '"}')
      dic = json.loads(s_json)
      for k in dic:
        if dic[k] == "True":
          l += [[s[:i]+'_'+k, True]]
        else:
          l += [[s[:i]+'_'+k, False]]
    elif s[i+2:] == "True":
      l += [[s[:i], True]]
    elif s[i+2:] == "False":
      l += [[s[:i], False]]
    else:
      l += [[s[:i]+'_'+s[i+2:], True]]
  return l


def parsebusinesshours(hourslist):
  hours = {"Mon":[None, None], "Tue":[None, None], "Wed":[None, None], 
           "Thu":[None, None], "Fri":[None, None], "Sat":[None, None], 
           "Sun":[None, None]}
  for s in hourslist:
    i = 0
    while(s[i] != " " and i < len(s)):
      i += 1
    j = i + 1
    while(s[j] != "-" and j < len(s)):
      j += 1
    hours[s[:3]] = [s[i+1:j], s[j+1:]]
  l = []
  for day in ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]:
    l += hours[day]
  return l

file_Business = open("Business.csv", "w")
Businesswriter = csv.writer(file_Business)
#Businesswriter.writerow(["businessID", "name", "neighborhood", "address", "city",
#                         "state", "postalCode", "latitude", "longitude", "stars", 
#                         "reviewCount", "isOpen"]) 

file_BusinessHours = open("BusinessHours.csv", "w")
BusinessHourswriter = csv.writer(file_BusinessHours)

file_AttrName = open("BusinessAttributeName.csv", "w")
AttrNamewriter = csv.writer(file_AttrName)
#AttrNamewriter.writerow(["attrName"])

file_CatName = open("BusinessCategoryName.csv", "w")
CatNamewriter = csv.writer(file_CatName)
#CatNamewriter.writerow(["catName"])


file_BusinessAttr = open("BusinessAttribute.csv", "w")
BusinessAttrwriter = csv.writer(file_BusinessAttr)
#BusinessAttrwriter.writerow(["businessID", "attrName", "attrValue"])

file_BusinessCat = open("BusinessCategory.csv", "w")
BusinessCatwriter = csv.writer(file_BusinessCat)
#BusinessCatwriter.writerow(["businessID", "catName"])

catset = set()
attrset = set()

for i, line in enumerate(open("yelp_academic_dataset_business.json", "r")):
#  if i >= 1:
#    break;
  data = json.loads(line)
  
  #Business
  Businesswriter.writerow([data["business_id"], data["name"], data["neighborhood"], 
                           data["address"], data["city"], data["state"], 
                           data["postal_code"], data["latitude"], data["longitude"], 
                           data["stars"], data["review_count"], data["is_open"]])

  #BusinessHours
  if data["hours"] is None:
    data["hours"] = []
  BusinessHourswriter.writerow([data["business_id"]] + parsebusinesshours(data["hours"]))
  
  #BusinessCategoryName
  if data["categories"] is not None:
    catset = catset.union(data["categories"])
    #BusinessCategory
    for cat in data["categories"]:
      BusinessCatwriter.writerow([data["business_id"], cat])  
  
  #BusinessAttributeName
  if data["attributes"] is not None:
    parsed = parseattr(data["attributes"])
    attrset = attrset.union([u[0] for u in parsed])
    #BusinessAttribute
    for j, attr in enumerate(parsed):
      BusinessAttrwriter.writerow([data["business_id"]]+parsed[j]) 

for cat in catset:
  CatNamewriter.writerow([cat])
for attr in attrset:
  AttrNamewriter.writerow([attr])  

