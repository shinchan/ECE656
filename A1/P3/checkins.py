#!/bin/python3

import json
import csv

def parsecheckins(chkinlist):
  chkins = {"Mon":[None]*24, "Tue":[None]*24, "Wed":[None]*24, 
           "Thu":[None]*24, "Fri":[None]*24, "Sat":[None]*24, 
           "Sun":[None]*24}
  for s in chkinlist:
    i = 0
    while(s[i] != ':' and i < len(s)):
      i += 1
    chkins[s[:3]][int(s[4:i])] = int(s[i+1:])
  l = []
  for day in ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]:
    l += chkins[day]
  return l


file_Checkins = open("Checkins.csv", "w")
Checkinswriter = csv.writer(file_Checkins)
#Checkinswriter.writerow(["text", "date", "likes", "businessID", "userID"])

for i, line in enumerate(open("yelp_academic_dataset_checkin.json", "r")):
#  if i >= 1000:
#    break;
  data = json.loads(line)
  
  #Checkins
  if data["time"] != None:
    Checkinswriter.writerow([data["business_id"]] + parsecheckins(data["time"]))
