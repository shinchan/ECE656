#!/bin/python3

import json
import csv

file_YelpUser = open("YelpUser.csv", "w")
YelpUserwriter = csv.writer(file_YelpUser)
#YelpUserwriter.writerow(["userID", "name", "reviewCount", "yelpingSince", "useful",
#                         "funny", "cool", "fans", "averageStars", "compliment_hot", 
#                         "compliment_more", "compliment_profile", "compliment_cute",
#                         "compliment_list", "compliment_note", "compliment_plain",   
#                         "compliment_cool", "compliment_funny", "compliment_writer", 
#                         "compliment_photos"])

file_UserFriend = open("UserFriend.csv", "w")
UserFriendwriter = csv.writer(file_UserFriend)
#UserFriendwriter.writerow(["userID", "friendUserID"])

file_UserEliteYear = open("UserEliteYear.csv", "w")
UserEliteYearwriter = csv.writer(file_UserEliteYear)
#UserEliteYearwriter.writerow(["userID", "year"])

for i, line in enumerate(open("yelp_academic_dataset_user.json", "r")):
#  if i >= 1000:
#    break;
  data = json.loads(line)
  
  #YelpUser
  YelpUserwriter.writerow([data["user_id"], data["name"], data["review_count"], 
                           data["yelping_since"], data["useful"], data["funny"], 
                           data["cool"], data["fans"], data["average_stars"], 
                           data["compliment_hot"], data["compliment_more"], 
                           data["compliment_profile"], data["compliment_cute"], 
                           data["compliment_list"], data["compliment_note"], 
                           data["compliment_plain"], data["compliment_cool"],
                           data["compliment_funny"], data["compliment_writer"], 
                           data["compliment_photos"]])
  
  #UserFriend
  if data["friends"] is not None:
    for friend in data["friends"]:
      if friend != "None":
        UserFriendwriter.writerow([data["user_id"], friend])
  
  #UserEliteYear
  if data["elite"] is not None:
    for year in data["elite"]:
      if year != "None":
        UserEliteYearwriter.writerow([data["user_id"], int(year)])
      
  
