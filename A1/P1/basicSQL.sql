--1a
SELECT *
FROM Department
WHERE location = 'Waterloo';

--1b
SELECT job, COUNT(empID)
FROM Employee
GROUP BY job
ORDER BY job;

--1c
SELECT empName, salary
FROM Employee
WHERE job = 'engineer';

--1d
SELECT job, AVG(salary)
FROM Employee
GROUP BY job
ORDER BY job;

--1e
START TRANSACTION;

CREATE VIEW Engineer AS
SELECT deptID, COUNT(*) AS engcnt
      FROM Employee
      WHERE job = 'engineer'
      GROUP BY deptID;

SELECT deptID, engcnt
FROM Engineer
WHERE engcnt = (SELECT MAX(engcnt)
                FROM Engineer
                );

DROP VIEW Engineer;
                
ROLLBACK;
      
--1f
START TRANSACTION;

CREATE TEMPORARY TABLE Engineer
SELECT deptID, engcnt/cnt AS percentage
FROM (SELECT deptID, COUNT(*) AS engcnt
      FROM Employee
      WHERE job = 'engineer'
      GROUP BY deptID
      ) AS Temp1
     NATURAL JOIN
     (SELECT deptID, COUNT(*) AS cnt
      FROM Employee
      GROUP BY deptID
     ) AS Temp2;
          
SELECT deptID, percentage
FROM Engineer
ORDER BY percentage DESC
LIMIT 1;

DROP TABLE Engineer;

ROLLBACK;

--2a
SELECT empName, empID
FROM Employee
WHERE empID NOT IN (SELECT empID FROM Assigned);

--2b
SELECT empName, job, role
FROM Employee NATURAL JOIN Assigned
WHERE job <> role;  

--2c
SELECT COUNT(*)
FROM Employee NATURAL JOIN Assigned
WHERE job = role;

--2d
SELECT projID, SUM(salary) AS totalSalary
FROM Employee NATURAL JOIN Assigned NATURAL JOIN Project
GROUP BY projID;

--2e
SELECT projID, SUM(salary) AS totalSalary
FROM Employee NATURAL LEFT OUTER JOIN (Assigned NATURAL JOIN Project)
GROUP BY projID;

--2f
SELECT empName, empID
FROM Employee NATURAL JOIN Assigned NATURAL JOIN Project
GROUP BY empID
HAVING COUNT(*) > 1;

--3a
UPDATE  Employee
SET salary = salary*1.1
WHERE empID IN (SELECT empID
                FROM Assigned NATURAL JOIN Project
                WHERE title = 'compiler'
                );


--3b
START TRANSACTION;

CREATE TEMPORARY TABLE Temp1(empID INT, job VARCHAR(100), location VARCHAR(100))
SELECT empID, job, location
FROM Department NATURAL JOIN Employee;

UPDATE  Employee
SET salary = salary*1.05
WHERE empID IN (SELECT empID
                FROM Temp1
                WHERE job = 'janitor' AND (NOT location = 'Waterloo')
                );

UPDATE Employee
SET salary = salary*1.08
WHERE empID IN (SELECT empID
                FROM Temp1
                WHERE location = 'Waterloo'
                );

DROP TABLE Temp1;                
                
COMMIT;

--3c
START TRANSACTION;

CREATE TEMPORARY TABLE Temp2(empID INT)
SELECT empID
FROM Employee NATURAL JOIN Assigned;

DELETE FROM Employee
WHERE deptID in (SELECT deptID FROM Department WHERE location = 'Kitchener') 
      AND (empID NOT IN (SELECT * FROM Temp2));

UPDATE Employee
SET deptID=(SELECT deptID FROM Department WHERE location = 'Waterloo')
WHERE deptID in (SELECT deptID FROM Department WHERE location = 'Kitchener');

DELETE FROM Department
WHERE location = 'Kitchener';

DROP TABLE Temp2; 

COMMIT;














































