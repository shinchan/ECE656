SELECT *
FROM Department
WHERE location = 'Waterloo';

SELECT job, COUNT(empID)
FROM Employee
GROUP BY job
ORDER BY job;

SELECT empName, salary
FROM Employee
WHERE job = 'engineer';

SELECT job, AVG(salary)
FROM Employee
GROUP BY job
ORDER BY job;

SELECT deptID,MAX(engcnt) 
FROM (SELECT deptID, COUNT(*) AS engcnt
      FROM Employee
      WHERE job = 'engineer'
      GROUP BY deptID
      ) Engineer;

SELECT deptID, MAX(engcnt/cnt) AS percentage      
FROM (SELECT deptID, COUNT(*) AS engcnt
 FROM Employee
 WHERE job = 'engineer'
 GROUP BY deptID
) AS Engineer
NATURAL JOIN
(SELECT deptID, COUNT(*) AS cnt
 FROM Employee
 GROUP BY deptID
) AS Total;

SELECT empName, empID
FROM Employee
WHERE empID NOT IN (SELECT empID FROM Assigned);

SELECT empName, job, role
FROM Employee NATURAL JOIN Assigned
WHERE job <> role;  

SELECT COUNT(*)
FROM Employee NATURAL JOIN Assigned
WHERE job = role;

SELECT projID, SUM(salary) AS totalSalary
FROM Employee NATURAL JOIN Assigned NATURAL JOIN Project
GROUP BY projID;

SELECT projID, SUM(salary) AS totalSalary
FROM Employee NATURAL LEFT OUTER JOIN (Assigned NATURAL JOIN Project)
GROUP BY projID;

SELECT empName, empID
FROM Employee NATURAL JOIN Assigned NATURAL JOIN Project
GROUP BY empID
HAVING COUNT(*) > 1;

UPDATE  Employee
SET salary = salary*1.1
WHERE empID IN (SELECT empID
                FROM Assigned NATURAL JOIN Project
                WHERE title = 'compiler'
                );


    
