import csv
import psycopg2

conn = psycopg2.connect("dbname=yelp user=gsdmember")

cur1 = conn.cursor()
cur1.execute("""SELECT DISTINCT businessID
                FROM (ValidYelpUserID 
                       NATURAL JOIN
                     (ValidBusinessID
                       NATURAL JOIN 
                     Review) AS TEMP1) AS TEMP2""")

cur2 = conn.cursor()
#cur2.execute("CREATE TABLE BusinessCategoryBinary(
#              CHAR(22) businessID REFERENCES Business(businessID),
#              BOOL food;
##             )")


for cnt, i in enumerate(cur1.fetchall()):
  if cnt % 1000==0:
    print(cnt)
  cur2.execute("SELECT catName FROM BusinessCategory WHERE businessID='"+i[0]+"'")
  catlst = cur2.fetchall()
#  print(catlst)
  catvector = [0]*11
  if ("Food",) in catlst:
    catvector[0] = 1
  if ("Health & Medical",) in catlst:
    catvector[1] = 1
  if ("Bars",) in catlst:
    catvector[2] = 1
  if ("Nightlife",) in catlst:
    catvector[3] = 1
  if ("Home Services",) in catlst:
    catvector[4] = 1
  if ("Beauty & Spas",) in catlst:
    catvector[5] = 1
  if ("Restaurants",) in catlst:
    catvector[6] = 1
  if ("Shopping",) in catlst:
    catvector[7] = 1
  if ("Automotive",) in catlst:
    catvector[8] = 1
  if ("Local Services",) in catlst:
    catvector[9] = 1
  if sum(catvector) == 0:
    catvector[10] = 1
  value = i + tuple(catvector)
#  print(value)
  cur2.execute("INSERT INTO BusinessCategoryBinary VALUES" + str(value))


conn.commit()


