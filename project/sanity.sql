-- userEliterYear must be no smaller than year of his/her first review
SELECT COUNT(*)
FROM  UserEliteYear NATURAL JOIN
        (SELECT userID, CAST(EXTRACT(year FROM yelpingSince) AS INT) as yelpingSince
         FROM YelpUser) AS TEMP
WHERE year < yelpingSince;

-- make sure that the date is sane
SELECT COUNT(*)
FROM UserEliteYear
WHERE year < 2004 or year > 2017;

SELECT COUNT(*)
FROM Review
WHERE date < '2004-01-01' or date > '2017-04-01';

SELECT COUNT(*)
FROM Tip
WHERE date < '2004-01-01' or date > '2017-04-01';

SELECT COUNT(*)
FROM YelpUser
WHERE yelpingSince < '2004-01-01' or yelpingSince > '2017-04-01';


-- The number of reviews a business receives must be no less than
-- the number of review on that business in the sample
SELECT COUNT(*)
FROM  (SELECT businessID, reviewCount FROM Business) AS TEMP1 NATURAL JOIN
        (SELECT businessID, COUNT(*) AS sampleReviewCount FROM Review GROUP BY businessID) AS TEMP2
WHERE reviewCount < sampleReviewCount;

-- The number of reviews a user made must be no less than
-- the number of review made by that user in the sample
SELECT COUNT(*)
FROM  (SELECT userID, reviewCount FROM YelpUser) AS TEMP1 NATURAL JOIN
        (SELECT userID, COUNT(*) AS sampleReviewCount FROM Review GROUP BY userID) AS TEMP2
WHERE reviewCount < sampleReviewCount;

-- create a table of business that violates the above sanity checks
CREATE TABLE InvalidBusiness AS (
SELECT businessID
FROM  (SELECT businessID, reviewCount FROM Business) AS TEMP1 NATURAL JOIN
        (SELECT businessID, COUNT(*) AS sampleReviewCount FROM Review GROUP BY businessID) AS TEMP2
WHERE reviewCount < sampleReviewCount
);
ALTER TABLE ADD InvalidBusiness PRIMARY KEY (businessID);

CREATE VIEW ValidBusiness AS(
SELECT *
FROM Business
WHERE businessID NOT IN InvalidBusiness
)
-- create a table of users that violates the above sanity checks
CREATE TABLE InvalidYelpUser AS (
SELECT userID
FROM  (SELECT userID, reviewCount FROM YelpUser) AS TEMP1 NATURAL JOIN
        (SELECT userID, COUNT(*) AS sampleReviewCount FROM Review GROUP BY userID) AS TEMP2
WHERE reviewCount < sampleReviewCount
);
ALTER TABLE ADD InvalidYelpUser PRIMARY KEY (userID);

CREATE VIEW ValidYelpUser AS(
SELECT *
FROM YelpUser
WHERE userID NOT IN InvalidYelpUser
)

-- t distribution 90% confidence interval
INSERT INTO InvalidBusiness (businessID)
SELECT businessID
FROM Review
WHERE businessID NOT IN (SELECT * FROM InvalidBusiness)
GROUP BY businessID
HAVING COUNT(*) > 100
       AND businessID NOT IN (SELECT * FROM InvalidBusiness)
       AND (ABS((AVG(stars)-(SELECT stars 
                             FROM Business
                             WHERE Review.businessID=Business.businessID)))=0
           OR ABS((AVG(stars)-(SELECT stars 
                               FROM Business
                               WHERE Review.businessID=Business.businessID)))/
              SQRT((AVG(stars*stars)-AVG(stars)^2)/COUNT(*)/(COUNT(*)-1)) > 1.8
           );

INSERT INTO InvalidBusiness (businessID)
SELECT businessID
FROM (SELECT businessID, stars AS recorded FROM Business) Temp1 
    NATURAL JOIN
     (SELECT businessID, AVG(stars) AS rawstars FROM Review GROUP BY businessID Having COUNT(*)<=100) Temp2
WHERE ABS(recorded - rawstars) > 1;       

INSERT INTO InvalidYelpUser (userID)
SELECT userID
FROM Review
GROUP BY userID
HAVING COUNT(*) > 100
       AND userID NOT IN (SELECT * FROM InvalidYelpUser)
       AND (ABS((AVG(stars)-(SELECT averageStars 
                             FROM YelpUser
                             WHERE Review.userID=YelpUser.userID)))=0
           OR ABS((AVG(stars)-(SELECT averageStars 
                               FROM YelpUser
                               WHERE Review.userID=YelpUser.userID)))/
              SQRT((AVG(stars*stars)-AVG(stars)^2)/COUNT(*)/(COUNT(*)-1)) > 1.8
           );

INSERT INTO InvalidYelpUser (userID)            
SELECT userID
FROM (SELECT userID, averageStars AS recorded FROM YelpUser) Temp1
    NATURAL JOIN
     (SELECT userID, AVG(stars) AS rawstars FROM Review GROUP BY userID 
      Having COUNT(*)<=100 AND userID NOT IN InvalidYelpUser) Temp2
WHERE ABS(recorded - rawstars) > 1;