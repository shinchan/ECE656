import csv

import psycopg2

from time import time
import numpy as np
import matplotlib.pyplot as plt

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale

import tensorflow as tf

conn = psycopg2.connect("dbname=yelp user=gsdmember")
cur1 = conn.cursor()
cur2 = conn.cursor()

with open('userStars.csv', 'r') as data_file:
  data_reader = csv.reader(data_file)
  label_reader = csv.reader(label_file)
  uid = []
  data = []
  for data_row in data_reader:       
    data += [[np.float(x) for x in data_row[1:]]]
    uid += data_row[data_row[0]] 
  data = np.array(data)
 
m, n = data.shape 
data = scale(data)
kmeans = MiniBatchKMeans(n_clusters=6).fit(data)
dic = []
for i in range(0, 8):
  dic += [[u for u,v in enumerate(kmeans.labels_) if v == i]]
diclen = [len(dic[i]) for i in len(dic)]


def batch(size):
  i = np.random.randint(8)
  x = []
  y = []
  for u, v in zip(np.random.randint(len(dic[i]), size=size), 
                  np.random.randint(len(dic[i]), size=size)):
    cur1.execute("""SELECT food, health, bars, nightlife, home, beauty, restaurants, 
                           shopping, automotive, local, others, Business.stars, Temp1.stars 
                    FROM BusinessCategoryBinary
                           NATURAL JOIN
                         (SELECT businessID, stars
                          FROM Review 
                          WHERE userID=""" + uid[sum(diclen[:i]+u)] + 
                          """ ORDER BY RANDOM() LIMIT 1) AS Temp1""")
    t = cur1.fetchone()
    x += [[i]+list(t[1:-1])]
    y = [t[-1]]
  return (x, y)
  #x = np.array(x)
  #y = np.array(y)

# neural network
# input=(usertype, businesstype, ##businessstar##)
x = tf.placeholder(tf.float32, shape = [None, 13])
# output=1-in-k vector of star 0.0-5.0
y_ = tf.placeholer(tf.float32, shape = [None, 11])

W1 = tf.Variable(tf.zeros[13, 16])
b1 = tf.Variable(tf.zeros[16])
h = tf.relu(tf.matmul(x, W1) + b1)

W2 = tf.Variable(tf.zeros[16, 11])
b2 = tf.Variable(tf.zeros[11])
y = tf.relu(tf.matmul(x, W2) + b2)

cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
train_step = tf.train.AdamOptimizer().minimize(cross_entropy)

