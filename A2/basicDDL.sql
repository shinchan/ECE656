DROP TABLE IF EXISTS Department; 
CREATE TABLE Department(
  deptID INT PRIMARY KEY,
  deptName VARCHAR(100)
  );

DROP TABLE IF EXISTS DepartmentLocation;
CREATE TABLE DepartmentLocation(
  deptID INT REFERENCES Department(deptID),
  streetNum INT NOT NULL,
  street VARCHAR(100) NOT NULL,
  city VARCHAR(100) NOT NULL,
  postCode VARCHAR(100) NOT NULL,
  PRIMARY KEY (deptID, streetNum, street, city, postCode)
  ); 

DROP TABLE IF EXISTS Employee; 
CREATE TABLE Employee(
  empID INT PRIMARY KEY,
  empFirstName VARCHAR(100) NOT NULL,
  empMiddleName VARCHAR(100),
  empLastName VARCHAR(100) NOT NULL,
  job VARCHAR(100) NOT NULL,
  salary INT NOT NULL
  );
CREATE TRIGGER EmployeeTrigger
BEFORE INSERT ON Employee
REFERENCING 
  OLD ROW AS Tuple
FOR EACH ROW
WHEN (Tuple.salary < 20000 OR Tuple.salary > 150000)
  
  
DROP TABLE IF EXISTS EmployeeDepartment;
CREATE TABLE EmployeeDepartment(
  empID INT NOT NULL REFERENCES Employee(empID),
  deptID INT NOT NULL REFERENCES Department(deptID),
  PRIMARY KEY (empID, deptID)
  );

DROP TABLE IF EXISTS Assigned;             
CREATE TABLE Assigned(
  empID INT REFERENCES Employee(empID),
  projID INT REFERENCES Project(projID),
  role VARCHAR(100) NOT NULL,
  PRIMARY KEY (empID, projID, role)
  );                 

DROP TABLE IF EXISTS Project;           
CREATE TABLE Project(
  projID INT PRIMARY KEY,
  title VARCHAR(100) NOT NULL,
  budget INT,
  funds INT
  );
  
